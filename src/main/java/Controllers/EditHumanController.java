package Controllers;

import HumanUtils.Human;
import HumanUtils.HumanColection;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static Controllers.MainTestAppController.editStage;

public class EditHumanController implements Initializable {
    public DatePicker birthdayDatePicker;
    public Button saveButton;
    public Button cancelButton;
    public TextField nameTextField;
    private int numStr = 0;
    private HumanColection humanColection = new HumanColection();
    private String name;
    private LocalDate date;
    private Integer age;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameTextField.setText(humanColection.getHumans().get(0).getName());
        birthdayDatePicker.setValue(humanColection.getHumans().get(0).getBirthday());
    }

    public void saveButtonOnMouseClicked(MouseEvent mouseEvent) {
        name = nameTextField.getText();
        date = birthdayDatePicker.getValue();
        age = LocalDate.now().getYear() - date.getYear();
        if (name.isEmpty()){
            Alert nameAlert = new Alert(Alert.AlertType.WARNING);
            nameAlert.setHeaderText(null);
            nameAlert.setContentText("Enter the name!");
            nameAlert.setTitle("Warning!");
            nameAlert.showAndWait();
        }
        else {
            humanColection.editHuman(numStr, new Human(name, age, date));
            editStage.hide();
        }

    }

    public void setNumStr(int numStr) {
        this.numStr = numStr;
    }

    public void cancelButtonOnMouseClicked(MouseEvent mouseEvent) {
        editStage.hide();
    }
}