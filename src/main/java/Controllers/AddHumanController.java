package Controllers;

import HumanUtils.Human;
import HumanUtils.HumanColection;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static Controllers.MainTestAppController.addStage;

public class AddHumanController implements Initializable {
    public Button saveButton;
    public Button cancelButton;
    public DatePicker birthdayPickerDate;
    public TextField nameTextField;
    private HumanColection humanColection = new HumanColection();
    private String name;
    private Integer age;
    private LocalDate date;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        birthdayPickerDate.setValue(LocalDate.now());
    }

    public void saveOnMouseButtonClicked(MouseEvent mouseEvent) throws IOException{
        name = nameTextField.getText();
        date = birthdayPickerDate.getValue();

        if (name.isEmpty()){
            Alert nameAlert = new Alert(Alert.AlertType.WARNING);
            nameAlert.setHeaderText(null);
            nameAlert.setContentText("Enter the name!");
            nameAlert.setTitle("Warning!");
            nameAlert.showAndWait();
        }
        else {
            age = LocalDate.now().getYear() - date.getYear();
            humanColection.addHuman(new Human(name, age, date));
            addStage.close();
        }
    }

    public void cancelOnMouseButtonClicked(MouseEvent mouseEvent) {
        addStage.close();
    }
}