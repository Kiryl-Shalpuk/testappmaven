package Controllers;

import HumanUtils.Human;
import HumanUtils.HumanColection;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

public class MainTestAppController implements Initializable {
    public TreeTableView<Human> humanTreeTableView;
    public TreeTableColumn<Human, String> nameColumnTable;
    public TreeTableColumn<Human, Integer> ageColumnTable;
    public TreeTableColumn<Human, Date> birthdayColumnTable;
    public Button addButton;
    public Button editButton;
    public Button deleteButton;
    public Button updateButton;
    protected static Stage addStage = new Stage();
    protected static Stage editStage = new Stage();
    private HumanColection humanColection = new HumanColection();
    private TreeItem<Human> root = new TreeItem<>(new Human("HumanUtils", 0, LocalDate.now()));

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        upDateTreeTableView();
    }

    public void addButtonOnMouseClicked(MouseEvent mouseEvent) throws IOException {
        Parent root2 = FXMLLoader.load(getClass().getResource("/views/AddHumanController.fxml"));
        addStage.setTitle("Add a new human");
        addStage.setResizable(false);
        addStage.centerOnScreen();
        addStage.setScene(new Scene(root2));
        addStage.showAndWait();
    }

    public void editButtonOnMouseClicked(MouseEvent mouseEvent) throws IOException {
        int stringHumanTable = humanTreeTableView.getSelectionModel().getSelectedIndex();
        if (stringHumanTable < 0){
            getHumanSelectAlert();
        }
        else {
            Parent root3 = FXMLLoader.load(getClass().getResource("/views/EditHumanController.fxml"));
            editStage.setTitle("Edit the human");
            addStage.setResizable(false);
            addStage.centerOnScreen();
            editStage.setScene(new Scene(root3));
            editStage.showAndWait();
            EditHumanController editHumanController = new EditHumanController();
            editHumanController.setNumStr(humanTreeTableView.getSelectionModel().getSelectedIndex());
        }
    }

    public void deleteButtonOnMouseClicked(MouseEvent mouseEvent) throws IOException {
        int stringHumanTable = humanTreeTableView.getSelectionModel().getSelectedIndex();
        if (stringHumanTable < 0){
            getHumanSelectAlert();
        }
        else {
            humanColection.deleteHuman(humanColection.getHumans().get(stringHumanTable));
            TreeItem<Human> selIt = humanTreeTableView.getSelectionModel().getModelItem(stringHumanTable);
            TreeItem<Human> parent = selIt.getParent();
            parent.getChildren().remove(selIt);
        }
    }

    public void doubleOnMouseCliked(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2){
            int stringHumanTable = humanTreeTableView.getSelectionModel().getSelectedIndex();
            LocalDate date = humanColection.getHumans().get(stringHumanTable).getBirthday();
            LocalDate dateNow = LocalDate.now();
            if (date.getMonth() == dateNow.getMonth() && date.getDayOfMonth() == dateNow.getDayOfMonth()){
                Alert doubleClkickAlert = new Alert(Alert.AlertType.WARNING);
                doubleClkickAlert.setTitle("Warning!");
                doubleClkickAlert.setHeaderText(null);
                doubleClkickAlert.setContentText(humanColection.getHumans().get(stringHumanTable).getName() + " has Birthday today!");
                doubleClkickAlert.showAndWait();
            }
        }
    }

    public void updateOnMouseClicked(MouseEvent mouseEvent) {
        upDateTreeTableView();
    }

    public void upDateTreeTableView(){
        root = new TreeItem<>(new Human("HumanUtils", 0, LocalDate.now()));
        root.setExpanded(true);
        humanColection.getHumans().stream().forEach((human) -> {
            root.getChildren().add(new TreeItem<>(human));
        });
        humanTreeTableView.setRoot(root);
        nameColumnTable.setCellValueFactory(new TreeItemPropertyValueFactory<Human, String>("name"));
        ageColumnTable.setCellValueFactory(new TreeItemPropertyValueFactory<Human, Integer>("age"));
        birthdayColumnTable.setCellValueFactory(new TreeItemPropertyValueFactory<Human, Date>("birthday"));
    }

    public static void getHumanSelectAlert(){
        Alert deleteAlert = new Alert(Alert.AlertType.WARNING);
        deleteAlert.setHeaderText(null);
        deleteAlert.setTitle("Warning!");
        deleteAlert.setContentText("Select the human!");
        deleteAlert.showAndWait();
    }
}