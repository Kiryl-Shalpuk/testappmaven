package HumanUtils;

public interface Editable {
    void addHuman(Human human);

    void editHuman(int num, Human human);

    void deleteHuman(Human human);
}