package HumanUtils;

import javafx.collections.FXCollections;
import java.time.LocalDate;
import java.util.List;

public class HumanColection implements Editable {
    private static List<Human> humans = FXCollections.observableArrayList();
    private HumanComparator humanComparator = new HumanComparator();

    @Override
    public void addHuman(Human human) {
        humans.add(human);
        sortHuman();
    }

    @Override
    public void editHuman(int num, Human human) {
        humans.set(num, human);
        sortHuman();
    }

    @Override
    public void deleteHuman(Human human) {
        humans.remove(human);
    }

    public List<Human> getHumans(){
        return humans;
    }

    public void sortHuman(){
        humans.sort(humanComparator);
    }

    public void startCreateHuman(){
        humans.add(new Human("Tom Hanks", 62, LocalDate.of(1956, 7, 9)));
        humans.add(new Human("Angelina Jolie", 43, LocalDate.of(1975, 6, 4)));
        humans.add(new Human("Meryl Streep", 69, LocalDate.of(1949, 6, 22)));
        humans.add(new Human("Johnny Depp", 55, LocalDate.of(1963, 6, 9)));
        humans.add(new Human("David Beckham", 43, LocalDate.of(1975, 5, 2)));
        humans.add(new Human("Paul McCartney", 76, LocalDate.of(1942, 6, 18)));
        humans.add(new Human("Rachel McAdams", 40, LocalDate.of(1978, 11, 17)));
        humans.add(new Human("Bill Gates", 63, LocalDate.of(1955, 10, 28)));
        humans.add(new Human("Jude Law", 45, LocalDate.of(1972, 12, 29)));
        sortHuman();
    }
}